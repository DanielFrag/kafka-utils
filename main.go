package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"gitlab.com/DanielFrag/kafka-utils/infra/kafka/producer"
	sv "gitlab.com/DanielFrag/kafka-utils/infra/kafka/schema-validator"
)

type Schemas struct {
	Key   uint32 `json:"key"`
	Value uint32 `json:"value"`
}

type MessageData struct {
	Topic   string      `json:"topic"`
	Key     string      `json:"key"`
	Value   interface{} `json:"value"`
	Schemas Schemas     `json:"schemas"`
}

var kafkaProducer producer.KafkaProducer

func main() {
	schemaValidator := sv.JsonSchemaValidator{}
	schemaValidator.Init()
	kafkaProducer = producer.KafkaProducer{}
	startError := kafkaProducer.Init(&schemaValidator)
	if startError != nil {
		fmt.Println(startError.Error())
		return
	}
	data, err := loadJson()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	err = writeData(data)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("Message sent")
	}
}

func writeData(data MessageData) error {
	value, marshalError := json.Marshal(data.Value)
	if marshalError != nil {
		return marshalError
	}
	writeError := kafkaProducer.SendMessage(data.Topic, []byte(data.Key), value, data.Schemas.Key, data.Schemas.Value)
	return writeError
}

func loadJson() (MessageData, error) {
	var messageData MessageData
	content, err := os.ReadFile("./data.json")
	if err != nil {
		return messageData, errors.New("could not load the json file")
	}
	json.Unmarshal(content, &messageData)
	fmt.Println(messageData)
	return messageData, nil
}
