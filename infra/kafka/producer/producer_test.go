package producer

import (
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type MockSchemaValidator struct {
	shouldReturnError bool
}

func (msv *MockSchemaValidator) Init() {}

func (msv *MockSchemaValidator) ValidateData(data []byte) error {
	if msv.shouldReturnError {
		return errors.New("Custom error 2 test")
	}
	return nil
}

var topicName string
var msv MockSchemaValidator
var consumer *kafka.Consumer
var producer KafkaProducer

func TestSendMessage(t *testing.T) {
	key := "key"
	value := "value"
	keySchemaId := uint32(5)
	valueSchemaId := uint32(6)
	sendMessageError := producer.SendMessage(topicName, []byte(key), []byte(value), keySchemaId, valueSchemaId)
	if sendMessageError != nil {
		t.Error(sendMessageError)
		return
	}
	timeout, _ := time.ParseDuration("20s")
	message, consumerError := consumer.ReadMessage(timeout)
	if consumerError != nil {
		t.Error(consumerError)
		return
	}
	consumedKeySchemaId := binary.BigEndian.Uint32(message.Key[1:5])
	consumedValueSchemaId := binary.BigEndian.Uint32(message.Value[1:5])
	if consumedKeySchemaId != keySchemaId || consumedValueSchemaId != valueSchemaId {
		t.Errorf("Error on write schema ids")
		return
	}
	consumedKey := string(message.Key[5:])
	consumedValue := string(message.Value[5:])
	if consumedKey != key || consumedValue != value {
		t.Errorf("Error on write data")
		return
	}
}

func TestSendMessageWithoutKey(t *testing.T) {
	value := "value"
	valueSchemaId := uint32(6)
	sendMessageError := producer.SendMessage(topicName, nil, []byte(value), 0, valueSchemaId)
	if sendMessageError != nil {
		t.Error(sendMessageError)
		return
	}
	timeout, _ := time.ParseDuration("20s")
	message, consumerError := consumer.ReadMessage(timeout)
	if consumerError != nil {
		t.Error(consumerError)
		return
	}
	if message.Key != nil {
		t.Errorf("The key should not be defined")
		return
	}
	consumedValueSchemaId := binary.BigEndian.Uint32(message.Value[1:5])
	if consumedValueSchemaId != valueSchemaId {
		t.Errorf("Error on write schema ids")
		return
	}
	consumedValue := string(message.Value[5:])
	if consumedValue != value {
		t.Errorf("Error on write data")
		return
	}
}

func TestMain(m *testing.M) {
	fmt.Println("TestMain - consumer.go")
	topicName = "test-billing-account-setup-producer"
	os.Setenv("CONFLUENT_BROKER", "localhost:9092")
	os.Setenv("ENVIRONMENT", "test")
	msv = MockSchemaValidator{}
	createTopic(os.Getenv("CONFLUENT_BROKER"), topicName)
	var err error
	consumer, err = buildConsumer(topicName, "test-billing-account-setup-producer-consumer-group")
	if err != nil {
		fmt.Printf("Error to instantiate the consumer: %v", err)
		os.Exit(1)
	}
	producer = KafkaProducer{}
	err = producer.Init(&msv)
	if err != nil {
		fmt.Printf("Error on producer init: %v", err)
		os.Exit(1)
	}
	exitCode := m.Run()
	consumer.Close()
	deleteTopic(os.Getenv("CONFLUENT_BROKER"), topicName)
	os.Exit(exitCode)
}

func createTopic(kafkaBrokerList string, topic string) {
	admin, err := kafka.NewAdminClient(&kafka.ConfigMap{"bootstrap.servers": kafkaBrokerList})
	if err != nil {
		panic((err))
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err = admin.CreateTopics(ctx, []kafka.TopicSpecification{{
		Topic:             topic,
		NumPartitions:     1,
		ReplicationFactor: 1,
	}})
	if err != nil {
		panic(err)
	}
}

func buildConsumer(topicName, groupId string) (*kafka.Consumer, error) {
	var newConsumerError error
	consumer, newConsumerError = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": os.Getenv("CONFLUENT_BROKER"),
		"group.id":          groupId,
		"auto.offset.reset": "earliest",
	})
	if newConsumerError != nil {
		return nil, newConsumerError
	}
	consumer.Subscribe(topicName, nil)
	return consumer, nil
}

func deleteTopic(kafkaBrokerList string, topic string) {
	admin, err := kafka.NewAdminClient(&kafka.ConfigMap{"bootstrap.servers": kafkaBrokerList})
	if err != nil {
		panic((err))
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err = admin.DeleteTopics(ctx, []string{topic})
	if err != nil {
		panic(err)
	}
}
