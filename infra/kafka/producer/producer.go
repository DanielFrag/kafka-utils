package producer

import (
	"encoding/binary"
	"errors"
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	schemavalidator "gitlab.com/DanielFrag/kafka-utils/infra/kafka/schema-validator"
)

type KafkaProducer struct {
	schemavalidator    schemavalidator.SchemaValidator
	kafkaProducer      *kafka.Producer
	shutdownInProgress bool
}

func (kp *KafkaProducer) Init(schemavalidator schemavalidator.SchemaValidator) error {
	if kp.kafkaProducer != nil {
		return errors.New("Producer already instantiated")
	}
	kp.schemavalidator = schemavalidator
	if kp.schemavalidator == nil {
		return errors.New("Schema validator not defined")
	}
	configMap, err := kp.buildKafkaConfigMap()
	if err != nil {
		return err
	}
	kp.kafkaProducer, err = kafka.NewProducer(&configMap)
	if err != nil {
		return err
	}
	return nil
}

func (kp *KafkaProducer) SendMessage(topicName string, key, value []byte, keySchemaId, valueSchemaId uint32) error {
	var err error
	if kp.kafkaProducer == nil {
		return errors.New("The producer was not defined")
	}
	var keyWithSchemaId, valueWithSchemaId []byte
	if key != nil {
		keyWithSchemaId, err = kp.buildMessageBuffer(key, keySchemaId)
		if err != nil {
			return err
		}
	}
	valueWithSchemaId, err = kp.buildMessageBuffer(value, valueSchemaId)
	if err != nil {
		return err
	}
	kafkaMessage := &kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topicName,
			Partition: kafka.PartitionAny,
		},
		Key:   keyWithSchemaId,
		Value: valueWithSchemaId,
	}
	kp.kafkaProducer.Produce(kafkaMessage, nil)
	producerEvent := <-kp.kafkaProducer.Events()
	switch ev := producerEvent.(type) {
	case *kafka.Message:
		if ev.TopicPartition.Error != nil {
			err = errors.New(fmt.Sprintf("Error on send message key %s, value %s to %v\n", string(ev.Key), string(ev.Value), ev.TopicPartition))
		}
	default:
		err = nil
	}
	return err
}

func (kp *KafkaProducer) buildMessageBuffer(data []byte, schemaId uint32) ([]byte, error) {
	dataSchemaId := make([]byte, 4)
	binary.BigEndian.PutUint32(dataSchemaId, schemaId)
	var valueWithSchema []byte
	valueWithSchema = append(valueWithSchema, byte(0))
	valueWithSchema = append(valueWithSchema, dataSchemaId...)
	valueWithSchema = append(valueWithSchema, data...)
	return valueWithSchema, kp.schemavalidator.ValidateData(valueWithSchema)
}

func (kp *KafkaProducer) buildKafkaConfigMap() (kafka.ConfigMap, error) {
	config := kafka.ConfigMap{}
	kafkaBrokerList := os.Getenv("CONFLUENT_BROKER")
	if kafkaBrokerList == "" {
		return nil, errors.New("The broker list is not defined. Set the environment variable BROKER_LIST")
	}
	config["bootstrap.servers"] = kafkaBrokerList
	securityProtocol := os.Getenv("KAFKA_SECURITY_PROTOCOL")
	if securityProtocol == "SASL_SSL" {
		config["security.protocol"] = "SASL_SSL"
		config["sasl.mechanism"] = "PLAIN"
		userName := os.Getenv("CONFLUENT_BROKER_API_KEY")
		password := os.Getenv("CONFLUENT_BROKER_API_SECRET")
		if userName == "" || password == "" {
			return nil, errors.New("Missing required properties username and password")
		}
		config["sasl.username"] = userName
		config["sasl.password"] = password
	} else if securityProtocol != "" {
		config["security.protocol"] = securityProtocol
	}
	environment := os.Getenv("ENVIRONMENT")
	if environment == "development" || environment == "test" {
		config["enable.ssl.certificate.verification"] = false
	}
	return config, nil
}
