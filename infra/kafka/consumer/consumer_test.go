package kafkaConsumer

import (
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type MockSchemaValidator struct {
	shouldReturnError bool
}

func (msv *MockSchemaValidator) Init() {}

func (msv *MockSchemaValidator) ValidateData(data []byte) error {
	if msv.shouldReturnError {
		return errors.New("Custom error 2 test")
	}
	return nil
}

var consumer *KafkaConsumer
var messageChannel chan SimpleMessage
var errorChannel chan SimpleMessage
var producer *kafka.Producer
var topicName string
var msv MockSchemaValidator

func TestConsumeMessage(t *testing.T) {
	t.Log("TestConsumeMessage")
	done := make(chan bool)
	keyStr := "62584f30ed933ab17028d5a1"
	valueStr := `{"foo":"sunda","bar":2}`
	var consumeErrorData error
	keyId := make([]byte, 4)
	binary.BigEndian.PutUint32(keyId, uint32(1))
	var key []byte
	key = append(key, byte(0))
	key = append(key, keyId...)
	key = append(key, []byte(keyStr)...)
	valueId := make([]byte, 4)
	binary.BigEndian.PutUint32(valueId, uint32(2))
	var value []byte
	value = append(value, byte(0))
	value = append(value, valueId...)
	value = append(value, []byte(valueStr)...)
	go func() {
		message := <-messageChannel
		if message.ErrorData != nil {
			consumeErrorData = message.ErrorData
		}
		if string(message.Key) != keyStr || string(message.Value) != valueStr {
			consumeErrorData = errors.New("Key or Value corrupted")
		}
		done <- true
	}()
	go func() {
		errorData := <-errorChannel
		consumeErrorData = errorData.ErrorData
		done <- true
	}()
	producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topicName,
			Partition: kafka.PartitionAny,
		},
		Key:   key,
		Value: value,
	}, nil)
	producer.Flush(3000)
	<-done
	if consumeErrorData != nil {
		t.Error(consumeErrorData)
		return
	}
}

func TestMain(m *testing.M) {
	fmt.Println("TestMain - consumer.go")
	topicName = "test-billing-account-setup-consumer"
	os.Setenv("CONFLUENT_BROKER", "localhost:9092")
	os.Setenv("CONSUMER_GROUP", "test-consumer-group-billing-account-setup")
	os.Setenv("CONSUMER_AUTO_OFFSET_RESET", "earliest")
	os.Setenv("ENVIRONMENT", "test")
	msv = MockSchemaValidator{}
	createTopic(os.Getenv("CONFLUENT_BROKER"), topicName)
	var producerError error
	producer, producerError = kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": os.Getenv("CONFLUENT_BROKER"),
	})
	if producerError != nil {
		fmt.Printf("Error to instantiate the producer: %v", producerError)
		os.Exit(1)
	}
	consumer = &KafkaConsumer{}
	messageChannel = make(chan SimpleMessage)
	errorChannel = make(chan SimpleMessage)
	consumer.StartConsumer(topicName, &msv, messageChannel, errorChannel)
	exitCode := m.Run()
	consumer.StopConsumer()
	deleteTopic(os.Getenv("CONFLUENT_BROKER"), topicName)
	os.Exit(exitCode)
}

func createTopic(kafkaBrokerList string, topic string) {
	admin, err := kafka.NewAdminClient(&kafka.ConfigMap{"bootstrap.servers": kafkaBrokerList})
	if err != nil {
		panic((err))
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err = admin.CreateTopics(ctx, []kafka.TopicSpecification{{
		Topic:             topic,
		NumPartitions:     1,
		ReplicationFactor: 1,
	}})
	if err != nil {
		panic(err)
	}
}

func deleteTopic(kafkaBrokerList string, topic string) {
	admin, err := kafka.NewAdminClient(&kafka.ConfigMap{"bootstrap.servers": kafkaBrokerList})
	if err != nil {
		panic((err))
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err = admin.DeleteTopics(ctx, []string{topic})
	if err != nil {
		panic(err)
	}
}
