package kafkaConsumer

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	schemavalidator "gitlab.com/DanielFrag/kafka-utils/infra/kafka/schema-validator"
)

var readTimeout time.Duration

type SimpleMessage struct {
	Key       []byte
	Value     []byte
	Topic     string
	Offset    int64
	Partition int32
	ErrorData error
}

type UseSchemaRegistry struct {
	Key   bool
	Value bool
}

type KafkaConsumer struct {
	kafkaConsumer      *kafka.Consumer
	messageChannel     chan<- SimpleMessage
	errorChan          chan<- SimpleMessage
	shutdownInProgress bool
	schemaValidator    schemavalidator.SchemaValidator
	logs               chan kafka.LogEvent
}

func (kc *KafkaConsumer) StartConsumer(topicName string, schemaValidator schemavalidator.SchemaValidator, messageChannel, errorChannel chan<- SimpleMessage) error {
	if messageChannel == nil || errorChannel == nil {
		return errors.New("Missing required channels")
	}
	readTimeout, _ = time.ParseDuration("3s")
	kc.messageChannel = messageChannel
	kc.errorChan = errorChannel
	consumerConfigMap, startConsumerError := kc.buildKafkaConfigMap()
	if startConsumerError != nil {
		return startConsumerError
	}
	kc.schemaValidator = schemaValidator
	kc.kafkaConsumer, startConsumerError = kafka.NewConsumer(&consumerConfigMap)
	if startConsumerError != nil {
		return startConsumerError
	}
	kc.kafkaConsumer.Subscribe(topicName, nil)
	go func() {
		for {
			if kc.shutdownInProgress {
				return
			}
			msg, err := kc.kafkaConsumer.ReadMessage(readTimeout)
			if err != nil {
				if err.(kafka.Error).Code() != kafka.ErrTimedOut {
					kc.errorChan <- SimpleMessage{
						ErrorData: err,
					}
				}
				continue
			}
			var key, value []byte
			if msg.Key != nil && len(msg.Key) > 5 {
				key = msg.Key[5:]
				err = kc.schemaValidator.ValidateData(msg.Key)
			}
			if msg.Value != nil && len(msg.Value) > 5 {
				value = msg.Value[5:]
				if err == nil {
					err = kc.schemaValidator.ValidateData(msg.Value)
				}
			}
			if err != nil {
				kc.errorChan <- SimpleMessage{
					Key:       key,
					Value:     value,
					Topic:     *msg.TopicPartition.Topic,
					Partition: msg.TopicPartition.Partition,
					Offset:    int64(msg.TopicPartition.Offset),
					ErrorData: err,
				}
				continue
			}
			kc.messageChannel <- SimpleMessage{
				Key:       key,
				Value:     value,
				Topic:     *msg.TopicPartition.Topic,
				Partition: msg.TopicPartition.Partition,
				Offset:    int64(msg.TopicPartition.Offset),
			}
		}
	}()
	go func() {
		for {
			log := <-kc.logs
			fmt.Println(log.String())
		}
	}()
	return nil
}

func (kc *KafkaConsumer) StopConsumer() {
	kc.shutdownInProgress = true
	time.Sleep(readTimeout * 2)
	kc.kafkaConsumer.Close()
	kc.kafkaConsumer = nil
}

func (kc *KafkaConsumer) buildKafkaConfigMap() (kafka.ConfigMap, error) {
	config := kafka.ConfigMap{}
	kafkaBrokerList := os.Getenv("CONFLUENT_BROKER")
	if kafkaBrokerList == "" {
		return nil, errors.New("The broker list is not defined. Set the environment variable BROKER_LIST")
	}
	config["bootstrap.servers"] = kafkaBrokerList
	group := os.Getenv("CONSUMER_GROUP")
	if group == "" {
		return nil, errors.New("The consumer group is not defined. Set the environment variable CONSUMER_GROUP")
	}
	config["group.id"] = group
	autoOffsetReset := os.Getenv("CONSUMER_AUTO_OFFSET_RESET")
	if autoOffsetReset != "" {
		config["auto.offset.reset"] = autoOffsetReset
	}
	securityProtocol := os.Getenv("KAFKA_SECURITY_PROTOCOL")
	if securityProtocol == "SASL_SSL" {
		config["security.protocol"] = "SASL_SSL"
		config["sasl.mechanism"] = "PLAIN"
		userName := os.Getenv("CONFLUENT_BROKER_API_KEY")
		password := os.Getenv("CONFLUENT_BROKER_API_SECRET")
		if userName == "" || password == "" {
			return nil, errors.New("Missing required properties username and password")
		}
		config["sasl.username"] = userName
		config["sasl.password"] = password
	} else if securityProtocol != "" {
		config["security.protocol"] = securityProtocol
	}
	environment := os.Getenv("ENVIRONMENT")
	if environment == "development" || environment == "test" {
		config["enable.ssl.certificate.verification"] = false
	}
	config["go.logs.channel.enable"] = true
	kc.logs = make(chan kafka.LogEvent)
	config["go.logs.channel"] = kc.logs
	return config, nil
}
