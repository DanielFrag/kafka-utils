# kafka utils

Projeto para postar mensagens em tópicos, integrando com o schema-registry

## Como usar

- Exportar variáveis de ambiente (usando o arquivo `env-example`)
  ```
  > . env-example
  ```
- Preencher o arquivo `data.json` com os dados a serem usados no envio do evento  
- Fazer o build do projeto
  ```
  > go build
  ```
- Rodar o executável gerado
